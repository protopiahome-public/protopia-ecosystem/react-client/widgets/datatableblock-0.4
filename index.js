import {useQuery, useLazyQuery, useMutation} from "@apollo/react-hooks";

import {
	getQueryName, getQueryArgs,
	queryCollection, querySingle,
	getChangeName, getInputTypeName, getMutationArgs,
	mutationAdd, mutationEdit, mutationDelete, getInput
} from "../../layouts/schema";
import UserContext from "../../layouts/userContext";
import {Router, Route, Switch, Redirect, withRouter} from 'react-router';


import React, { Component, Fragment, useContext, useState } from "react";
import DataTable from "./DataTable";

const DataTableBlock = function (props) {

	let context = useContext(UserContext);
	let data_type = props.data_type;

	let query_name = getQueryName(data_type)
	const query_args = getQueryArgs(data_type)

	let query = queryCollection( data_type, query_name, query_args );

	const mutation_name = getChangeName( data_type );
	const input_type_name = getInputTypeName( data_type );
	const mutation_args = getMutationArgs( data_type );
	let change_mutation = mutationEdit( data_type, mutation_name, input_type_name, mutation_args );

	let delete_mutation = mutationDelete( data_type );

	if (props.query) {
		query = props.query
	}
	if (props.change_mutation) {
		change_mutation = props.change_mutation
	}
	if (props.query_name) {
		query_name = props.query_name
	}

	//let [state, setState] = useState({});
	let [load, {loading, data:query_data}] = useLazyQuery(query, {client: props.client});
	const [edit_mutation_hook] = useMutation(change_mutation, {client: props.client, onCompleted: props.onChange});
	const [delete_mutation_hook] = useMutation(delete_mutation, {client: props.client, onCompleted: props.onChange});
	let data = {};

	function onSave(state) {
		let input = getInput(state, props.data_type);
		edit_mutation_hook({
			variables:
				{
					"id": this.ID,
					"input": input
				}
		})
	}

	// see: https://www.apollographql.com/docs/react/v2.4/essentials/mutations/
	//const fills = dt ? dt.admin_data.fill : ["#4580E6","#1F4B99"];
	//const fills = ["#3f586b", "#293742"];
	const fills = ["transparent", "transparent"];

	//console.log(query);
	return <DataTable
		theadColor = {fills[0]}
		trColor = {fills[1]}
		data_type = { data_type }
		query_name = {query_name}
		query = {query}
		mutation_name={mutation_name}
		change_mutation = {change_mutation}
		mutation_delete = {delete_mutation}
		route={props.route}
		isList={props.is_list}
	/>;

}

export default DataTableBlock;